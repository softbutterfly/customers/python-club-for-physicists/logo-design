# Logo Design: Python Club for Physicists

## Motivación

El isotipo original es una composición de dos imágenes:

1. El isotipo del lenguaje de programación Python
2. La representación esquematica de la refracción de la luz y su consecuente descomposisión.

La composisión realizada transmite el objetivo base del *Python Club for Physicists*: El uso de lenguaje del programación Python en el desarrollo de la investigación física; sin embargo gráfica y estéticamente, esta representación no está bien lograda.

## Ganador!

Final mente despues de la evaluación, el isologo elegido para representar al club de *Python Club for Physicists* es la propuesta 2 (Ver propuestas en la siguiente sección).

![proposal-2]

Adicional mente se ha preparador un [poster] donde se presentan las especificacione y usos del logotipo, el imagotipo y el isologo *Python Club for Physicists*.

## Propuestas

Las propuestas a continuación procuran mantener el espíritu del isotipo original, refinando su representación grafica y el logotipo del *Python Club for Physicists*.

El logotipo del *Python Club for Physicists*, es el nombre mismo escrito en color blanco sobe fondo negro empleando la tipografía [Hack] con grosor **bold**.

Sobre el logotipo se coloca el isotipo del cual se presetan a continuación 3 propuestas que muestran la composicion del isologo.

### Propuesta 1

El isotipo de Python actua como el prisma en el experimento de la refracción de la luz. Esta se desconmopne en sus 7 colores caracteristicos al interior del isotipo de Python y sale él.

![proposal-1]

### Propuesta 2

El isotipo de Python actua como el prisma en el experimento de la refracción de la luz. Esta se desconmopne en sus 7 colores caracteristicos antes de ingresar al isotipo de Python, al otro extremo la luz sale descompuesta y con mayor ancho mostrando un quiebre caracteristico de la luz que sale del prisma.

![proposal-2]

### Propuesta 3

El isotipo de Python de pthon se inscribe en una representacion del experimento de la refracción de la luz. Esta propuesta es una vectorización y esquematización de lo que se deseo representar en el isotipo original.

![proposal-3]

[Hack]: https://github.com/source-foundry/Hack
[proposal-1]: ./proposal-1/pcfp-black-rect.png
[proposal-2]: ./proposal-2/pcfp-black-rect.png
[proposal-3]: ./proposal-3/pcfp-black-rect.png
[poster]: ./specs/poster.pdf
